class M {
    B b;
}

class A extends C {
    M m;

    public void metA() {

    }

    A() {
        M m = new M();
    }
}

class C {

}

class L {
    M m;
    private int a;

    public void i(X x) {

    }
}

class X {

}

class B {
    public void metB() {

    }
}