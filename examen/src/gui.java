import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// extend and then minimize the window to see it properly :)
class gui {

    //Creating Static variables
    static JTextField text1;
    static JTextField text2;

    static JButton submit_btn;
    static JTextArea output_txtArea;


    public static void main(String args[]) {

        JFrame frame = new JFrame("PRODUCT");
        frame.setVisible(true);
        frame.setBounds(200, 100, 650, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        Container c = frame.getContentPane();
        c.setLayout(null);
        c.setBackground(Color.gray);


        Font f = new Font("Arial", Font.BOLD, 20);


        JLabel heading_lbl = new JLabel();
        heading_lbl.setBounds(250, 5, 200, 40);
        heading_lbl.setText("<html><font><u><b>Product</b></u></html>");
        heading_lbl.setVisible(true);

        heading_lbl.setFont(f);


        Font f1 = new Font("Arial", Font.BOLD, 14);


        JLabel text1_lbl = new JLabel("Number 1 : ");
        text1_lbl.setBounds(50, 80, 100, 30);
        text1_lbl.setVisible(true);


        text1 = new JTextField();
        text1.setBounds(180, 80, 180, 30);


        JLabel text2_lbl = new JLabel("Number 2 : ");
        text2_lbl.setBounds(50, 120, 150, 30);


        text2 = new JTextField();
        text2.setBounds(180, 120, 180, 30);


        // Setting Cursor for components
        Cursor curs = new Cursor(Cursor.HAND_CURSOR);


        submit_btn = new JButton("Product");
        submit_btn.setBounds(200, 170, 120, 40);

        submit_btn.setCursor(curs);  // Applying hand cursor on the button


        submit_btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                submit_action(event);
            }
        });


        output_txtArea = new JTextArea();
        output_txtArea.setBounds(380, 80, 200, 70);


        text1_lbl.setFont(f1);
        text2_lbl.setFont(f1);


        text1.setFont(f1);
        text2.setFont(f1);

        submit_btn.setFont(f1);
        output_txtArea.setFont(f1);


        c.add(heading_lbl);
        c.add(text1_lbl);
        c.add(text2_lbl);


        c.add(text1);
        c.add(text1);
        c.add(text2);

        c.add(submit_btn);
        c.add(output_txtArea);
    }


    public static void submit_action(ActionEvent event) {
        double a;
        String name = text1.getText();
        String fname = text2.getText();
        a = Integer.parseInt(fname) * Integer.parseInt(name);
        output_txtArea.setText(" The product is:   " + a);

    }

}